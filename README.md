# Log Server

Server to receive `logging.LogRecord` objects as sent by the built-in `logging.handlers.SocketHandler` and web interface to view log records. Created in Windows, could be adapted to Linux.

This is unsafe, you better trust anything that sends log records to this.

Setup with Postgresql on Windows in Git shell

```bash
$ createdb logserver
$ virtualenv venv
$ source venv/Scripts/activate
$ pip install -U -r requirements.txt
$ export FLASK_APP=wsgi.py
$ flask shell
>>> db.create_all()
```

Minimum Receiver and Web app instance folder config

```bash
$ cat instance/server-config.py
```

```python        
SECRET_KEY = 'SHHH!'

SQLALCHEMY_DATABASE_URI = 'postgresql:///logserver'
SQLALCHEMY_TRACK_MODIFICATIONS = False
```

Start LogRecord Receiver

```bash
$ python -m logserver.receiver
```

Configure Logging in senders
```python
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'simple': {
            'datefmt': '%Y-%m-%d %H:%M:%S %p',
            'format': '%(asctime)s:%(name)s:%(levelname)s:%(message)s'
        },
    },
    'handlers': {
        'logserver': {
            'class': 'logging.handlers.SocketHandler',
            'host': 'dotted ip of receiver (printed when started)',
            'port': 9020,
        },
    },
    'loggers': {
        'logger.name': {
            'handlers': ['logserver'],
            'level': 'DEBUG',
        },
    }
}
logging.config.dictConfig(LOGGING)
```

Start Development Flask App Log viewer

```bash
$ flask run
```

Or point your web server at `wsgi.py`.