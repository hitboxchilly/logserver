import logging
from extensions import db

from databaselogrecord.models import DatabaseLogRecord

def save_logrecord(record):
    """
    Save logging.LogRecord to database.
    """
    logrecord = DatabaseLogRecord(name=record.name, logrecord=record)
    db.session.add(logrecord)
    db.session.commit()

def get_logger_names():
    """
    Get all the unique logger names from database.
    """
    query = (DatabaseLogRecord.query
             .with_entities(DatabaseLogRecord.name)
             .order_by(DatabaseLogRecord.name)
             .distinct())
    appnames = [ tup[0] for tup in query ]
    return appnames

def get_logrecords(loggername, levelno=logging.NOTSET, page=1, per_page=25):
    """
    Return all database log record for loggername.
    """
    return (DatabaseLogRecord.query
            .filter(DatabaseLogRecord.name == loggername,
                    DatabaseLogRecord.levelno >= levelno)
            .order_by(DatabaseLogRecord.created)
            .paginate(page, per_page))
