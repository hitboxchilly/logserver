import logging
import logging.config
import logging.handlers
import os
import pickle
import select
import struct

from flask.config import Config

try:
    import socketserver
except ImportError:
    import SocketServer as socketserver

from app import create_app
from databaselogrecord import DatabaseLogRecord
from extensions import db

def get_logger():
    return logging.getLogger('logserver.receiver')

class LogRecordStreamHandler(socketserver.StreamRequestHandler):
    """
    Handler for a streaming logging request. This saves the logrecord received
    to the database.
    """

    def handle(self):
        """
        Handle multiple requests - each expected to be a 4-byte length,
        followed by the LogRecord in pickle format. Logs the record according
        to whatever policy is configured locally.
        """
        while True:
            chunk = self.connection.recv(4)
            if len(chunk) < 4:
                break
            slen = struct.unpack('>L', chunk)[0]
            chunk = self.connection.recv(slen)
            while len(chunk) < slen:
                chunk = chunk + self.connection.recv(slen - len(chunk))
            obj = self.unPickle(chunk)
            record = logging.makeLogRecord(obj)
            self.handleLogRecord(record)

    def unPickle(self, data):
        return pickle.loads(data)

    def handleLogRecord(self, record):
        logger = get_logger()
        logger.debug('handleLogRecord: %s', record)
        # if a name is specified, we use the named logger rather than the one
        # implied by the record.
        if self.server.logname is not None:
            name = self.server.logname
        else:
            name = record.name

        app = create_app()
        with app.app_context():
            dblogrecord = DatabaseLogRecord(name=name, created=record.created,
                                            levelname=record.levelname,
                                            levelno=record.levelno,
                                            logrecord=record)
            db.session.add(dblogrecord)
            db.session.commit()

class LogRecordSocketReceiver(socketserver.ThreadingTCPServer):
    """
    Simple TCP socket-based logging receiver suitable for testing.
    """

    allow_reuse_address = True

    def __init__(self, host,
                 port=logging.handlers.DEFAULT_TCP_LOGGING_PORT,
                 handler=LogRecordStreamHandler):
        socketserver.ThreadingTCPServer.__init__(self, (host, port), handler)
        self.abort = 0
        self.timeout = 1
        self.logname = None

    def serve_until_stopped(self):
        abort = 0
        while not abort:
            rd, wr, ex = select.select([self.socket.fileno()],
                                       [], [],
                                       self.timeout)
            if rd:
                self.handle_request()
            abort = self.abort

def gethostip():
    import socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    rv = s.getsockname()[0]
    s.close()
    return rv

def main():
    """
    Start logging receiver.
    """
    from argparse import ArgumentParser

    parser = ArgumentParser(prog='receiver', description=main.__doc__)
    parser.add_argument('--host', default=gethostip(),
                        help='Host name. [%(default)s].')
    parser.add_argument('--port',
                        default=logging.handlers.DEFAULT_TCP_LOGGING_PORT,
                        type=int, help='Port number. [%(default)s].')
    parser.add_argument('--level', default='debug',
                        choices=['critical', 'debug', 'error', 'info', 'warning'],
                        help='Logging receiver (server) logging level. [%(default)s].')
    args = parser.parse_args()

    logging.basicConfig()

    logger = get_logger()

    # find and load instance dir config
    up = os.path.dirname
    instance_dir = os.path.join(up(up(os.path.abspath(__file__))), 'instance')
    if os.path.exists(instance_dir):
        config = Config(instance_dir)
        config.from_pyfile('server-config.py')
        if 'LOGGING' in config:
            logging.config.dictConfig(config['LOGGING'])

    logger.setLevel(getattr(logging, args.level.upper()))

    tcpserver = LogRecordSocketReceiver(args.host, args.port)
    logger.debug('Starting TCP server %s:%s, CTRL+C to stop...', *tcpserver.server_address)
    tcpserver.serve_until_stopped()

if __name__ == '__main__':
    main()
