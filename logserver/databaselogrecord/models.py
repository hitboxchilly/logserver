from sqlalchemy import Column, Float, Integer, PickleType, String

from ..extensions import db

class DatabaseLogRecord(db.Model):

    __tablename__ = 'logrecords'

    id = Column(Integer, primary_key=True)

    name = Column(String, index=True)
    created = Column(Float)
    levelname = Column(String, index=True)
    levelno = Column(Integer, index=True)

    logrecord = Column(PickleType)
