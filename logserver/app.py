DEFAULT_FORMAT_OPTIONS = [('%Y-%m-%d %I:%M:%S %p', 'Long')]

LEVELS = [(50, 'CRITICAL'), (40, 'ERROR'), (30, 'WARNING'), (20, 'INFO'), (10, 'DEBUG'), (0, 'NOTSET')]

def create_app():
    import logging

    from flask import Flask, render_template, request, session

    from extensions import db
    import operations

    app = Flask(__name__, instance_relative_config=True)
    app.config.from_pyfile('server-config.py')

    app.config['FORMAT_OPTIONS'] = [ (unicode(value), name) for value,name in DEFAULT_FORMAT_OPTIONS + app.config.get('FORMAT_OPTIONS', []) ]

    db.init_app(app)

    @app.before_first_request
    def clear_session():
        session.clear()

    @app.context_processor
    def inject_format_options():
        return dict(all_format_options=app.config['FORMAT_OPTIONS'],
                    all_levels=LEVELS)
    @app.route('/')
    def index():
        loggernames = operations.get_logger_names()
        return render_template('index.html', loggernames=loggernames)

    @app.route('/<loggername>')
    @app.route('/<loggername>/<int:page>')
    def log(loggername, page=1):

        def resolve_arg(key, default=None):
            """
            try query string, then session and finally default.
            """
            return request.args.get(key, session.get(key, default))

        format_options = app.config['FORMAT_OPTIONS']

        session['per_page'] = int(resolve_arg('per_page', 30))
        session['levelno'] = int(resolve_arg('levelno', logging.INFO))

        selected = session['selected_format_option'] = resolve_arg('selected_format_option', format_options[0][0])

        values = (value for value,name in format_options)
        for value in values:
            if selected == value:
                break
        else:
            raise RuntimeError('Format option %r not available in %r.' % (selected, format_options))

        pagination = operations.get_logrecords(loggername, session['levelno'], page, session['per_page'])
        return render_template('log.html', loggername=loggername, pagination=pagination)

    @app.shell_context_processor
    def f():
        from extensions import db
        from databaselogrecord import DatabaseLogRecord
        return locals()

    import time

    @app.context_processor
    def strftime_processor():
        def strftime(created):
            """
            Convert LogRecord.created float time to formatted local time.
            """
            return time.strftime(session['selected_format_option'], time.localtime(created))
        return dict(strftime=strftime)

    return app
