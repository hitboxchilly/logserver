"""added level name and number

Revision ID: 9169c62aafa0
Revises: Nothing
Create Date: 2017-04-24 23:55:44.556000

"""
import sqlalchemy as sa

from alembic import op
from sqlalchemy import Column, Float, Integer, PickleType, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

# revision identifiers, used by Alembic.
revision = '9169c62aafa0'
down_revision = None
branch_labels = None
depends_on = None

Session = sessionmaker()
Base = declarative_base()

class LogRecord(Base):
    """
    Just enough of the DatabaseLogRecord class to update the `levelname` and `levelno`.
    """
    __tablename__ = 'logrecords'
    id = Column(Integer, primary_key=True)
    levelname = Column(String, index=True)
    levelno = Column(Integer, index=True)
    logrecord = Column(PickleType)


def upgrade():
    op.add_column('logrecords', sa.Column('levelname', sa.String(), nullable=True))
    op.add_column('logrecords', sa.Column('levelno', sa.Integer(), nullable=True))
    op.create_index(op.f('ix_logrecords_levelname'), 'logrecords', ['levelname'], unique=False)
    op.create_index(op.f('ix_logrecords_levelno'), 'logrecords', ['levelno'], unique=False)
    op.create_index(op.f('ix_logrecords_name'), 'logrecords', ['name'], unique=False)

    session = Session(bind=op.get_bind())
    for logrecord in session.query(LogRecord):
        logrecord.levelname = logrecord.logrecord.levelname
        logrecord.levelno = logrecord.logrecord.levelno

    session.commit()


def downgrade():
    op.drop_index(op.f('ix_logrecords_name'), table_name='logrecords')
    op.drop_index(op.f('ix_logrecords_levelno'), table_name='logrecords')
    op.drop_index(op.f('ix_logrecords_levelname'), table_name='logrecords')
    op.drop_column('logrecords', 'levelno')
    op.drop_column('logrecords', 'levelname')
