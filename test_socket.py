from logserver.receiver import gethostip

def main():
    """
    Test sending logs to socket server.
    """
    import argparse
    import logging.handlers

    parser = argparse.ArgumentParser(prog='test_socket', description=main.__doc__)

    parser.add_argument('messages', nargs='+', help='Messages to send.')

    parser.add_argument('--name', default='test_socket', help='Logger name. [%(default)s].')
    parser.add_argument('--level', default='info',
                        choices=['info', 'debug', 'warn', 'info', 'error'],
                        help='Logger level. [%(default)s].')

    parser.add_argument('--host', default=gethostip(), help='Host name. [%(default)s].')
    parser.add_argument('--port',
                        default=logging.handlers.DEFAULT_TCP_LOGGING_PORT,
                        type=int, help='Port number. [%(default)s].')

    args = parser.parse_args()

    logging.basicConfig()

    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.DEBUG)
    socketHandler = logging.handlers.SocketHandler(args.host, args.port)
    rootLogger.addHandler(socketHandler)

    logger = logging.getLogger(args.name)
    logfunc = getattr(logger, args.level)

    for message in args.messages:
        logfunc(message)

if __name__ == '__main__':
    main()
