import os
import sys

THISDIR = os.path.dirname(__file__)

for subdir in ('Scripts', 'bin'):
    path = os.path.join(THISDIR, 'venv', subdir, 'activate_this.py')
    if os.path.exists(path):
        execfile(path, dict(__file__=path))
        break
else:
    raise RuntimeError('Unable to find the activate_this.py script.')

sys.path.append(THISDIR)

from logserver.app import create_app

class PrefixMiddleware(object):

    def __init__(self, app, prefix=''):
        self.app = app
        self.prefix = prefix

    def __call__(self, environ, start_response):

        if environ['PATH_INFO'].startswith(self.prefix):
            environ['PATH_INFO'] = environ['PATH_INFO'][len(self.prefix):]
            environ['SCRIPT_NAME'] = self.prefix
            return self.app(environ, start_response)
        else:
            start_response('404', [('Content-Type', 'text/plain')])
            return ["This url does not belong to the app.".encode()]

app = create_app()

if app.debug:
    app.wsgi_app = PrefixMiddleware(app.wsgi_app, prefix='/logserver')

application = app
